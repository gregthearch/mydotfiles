# To support cd on exit
LFCD="$HOME/.config/lf/lfcd.sh"  #  pre-built binary, make sure to use absolute path
if [ -f "$LFCD" ]; then
    source "$LFCD"
    alias lf="lfcd"
fi


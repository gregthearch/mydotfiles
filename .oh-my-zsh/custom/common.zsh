# path
export PATH=$PATH:$HOME/bin:$HOME/.cargo/bin:$HOME/.local/bin

# aliases
alias a='alias'
alias b='bat'
alias h='history'
alias ls='lsd'
alias l='ls'
alias ll='ls -l'
alias la='ls -la'
alias lt='ls -lat'
alias ltree='ls --tree'
alias v='nvim'
alias m='micro'

# NeoVim as default editor
export EDITOR='nvim'

# bat theme
export BAT_THEME='Monokai Extended Origin'

# show hidden files in autocomplete
setopt globdots

# vi mode
bindkey -v
bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward

# have some fun
colorscript random

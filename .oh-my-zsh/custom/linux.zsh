# alias vpnup="sudo nmcli connection up str-syd102_a363893"
# alias vpndown="sudo nmcli connection down str-syd102_a363893"
alias vpnup="sudo nmcli connection up str-syd101_pptp"
alias vpndown="sudo nmcli connection down str-syd101_pptp"
alias vpnbounce='vpndown && vpnup'
alias con="nmcli connection"
alias qbtrestart='sudo systemctl restart qbittorrent-nox@do.service'
alias qbtstop='sudo systemctl stop qbittorrent-nox@do.service'
alias qbtstart='sudo systemctl start qbittorrent-nox@do.service'
alias qbtstatus='systemctl status qbittorrent-nox@do.service'

# to backup dotfiles w/ git
alias bare='/usr/bin/git --git-dir=$HOME/repos/mydotfiles --work-tree=$HOME'
alias bareadd='bare add '
alias barecommit='bare commit -m '
alias barepush='bare push --set-upstream git@gitlab.com:gregthearch/mydotfiles.git'

# fzf configuration
export DISABLE_FZF_AUTO_COMPLETION="false"
export DISABLE_FZF_KEY_BINDINGS="false"
export FZF_BASE='/usr/share/fzf'
export FZF_DEFAULT_COMMAND='fd --hidden --follow --exclude ".git"'
export FZF_DEFAULT_OPTS="--layout=reverse --exact --color=query:#F08080,hl+:#F08080,hl:#F08080:bold,fg+:#FFFF00,prompt:#3FFf3F,pointer:#FFFF00 --height 100% --border --info inline --border-label `pwd`"
export FZF_CTRL_T_COMMAND="fd --hidden --follow"
export FZF_CTRL_T_OPTS="--preview 'fzf-previewer {}'"
export FZF_CTRL_R_OPTS="--border-label '<ctrl-r fzf>'"
# Use ~~ as the trigger sequence instead of the default **
export FZF_COMPLETION_TRIGGER='~~'
# Options to fzf command
export FZF_COMPLETION_OPTS=''
# Use fd (https://github.com/sharkdp/fd) for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}
# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}
# Advanced customization of fzf options via _fzf_comprun function
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
# _fzf_comprun() {
#   local command=$1
#   shift
#   case "$command" in
#     cd)           fzf --preview 'tree -C {} | head -200'   "$@" ;;
#     export|unset) fzf --preview "eval 'echo \$'{}"         "$@" ;;
#     ssh)          fzf --preview 'dig {}'                   "$@" ;;
#     *)            fzf --preview 'bat -n --color=always {}' "$@" ;;
#   esac
# }
_fzf_comprun() {
  shift
  fzf --preview 'fzf-previewer {}' "$@"
}

alias fzp="fd --hidden | fzf --exact --preview 'fzf-previewer {}'"

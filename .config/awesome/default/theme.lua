---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local gears = require("gears")
local themes_path = "~/.config/awesome/"

local theme = {}
local font_name = "JetBrainsMono Nerd Font"
-- local font_name = "IosevkaNerdFont"
theme.font = font_name .. " 10"

local shade1 = "#006064"
local shade2 = "#00838F"
local shade3 = "#0097A7"
local shade4 = "#00ACC1"
local shade5 = "#00BCD4"
local shade6 = "#26C6DA"
local shade7 = "#4DD0E1"
local shade8 = "#80DEEA"
local green = "#9ece6a"
local orange = "#ff9e64"
local magenta = "#FF7ED8"
local white = "#f0f0f0"
local yellow = "#ffff00"
local blue = "#7aa2f7"
local cyan = shade7
local red = "#ff6c6b"
local black = "#2B2139"
local darkgray = "#4d4d4d"
local lightgray = "#565f89"

local default_bg = black
local default_fg = white

theme.bg_normal = default_bg
theme.bg_focus = default_bg
theme.bg_urgent = default_bg
theme.bg_minimize = default_bg
theme.bg_systray = default_bg

theme.fg_normal = default_fg
theme.fg_focus = blue
theme.fg_urgent = red
theme.fg_minimize = lightgray

theme.useless_gap = dpi(3)
theme.border_width = dpi(2)
theme.border_normal = default_bg
theme.border_focus = blue
theme.border_marked = red

theme.enable_spawn_cursor = true

-- text prefixes
theme.tasklist_plain_task_name = false
theme.tasklist_maximized = "󰁌 "
theme.tasklist_ontop = "󰕕 "
theme.tasklist_above = " "
theme.tasklist_below = " "
theme.tasklist_floating = "󰉨 "
theme.tasklist_sticky = " "
theme.tasklist_maximized_horizontal = "󰡎 "
theme.tasklist_maximized_vertical = "󰡏 "
-- font
theme.tasklist_font = font_name .. " 9"
-- default window
theme.tasklist_bg_normal = default_bg
theme.tasklist_fg_normal = default_fg
-- theme.tasklist_shape_border_color = "transparent"
theme.tasklist_shape_border_color = default_bg
theme.tasklist_shape_border_width = dpi(0)
-- for underline (custom job) / shape_border_width must be 0 for underline to
-- work
theme.tasklist_underline_width = dpi(2)
-- focus windows
theme.tasklist_bg_focus = default_bg
theme.tasklist_fg_focus = blue
theme.tasklist_shape_border_color_focus = blue
-- theme.tasklist_shape_border_width_focus = theme.tasklist_shape_border_width
-- minimized windows
theme.tasklist_bg_minimize = theme.tasklist_bg_normal
theme.tasklist_fg_minimize = lightgray
theme.tasklist_shape_border_color_minimized = theme.tasklist_shape_border_color
-- urgent windows
theme.tasklist_bg_urgent = theme.tasklist_bg_normal
theme.tasklist_fg_urgent = red
theme.tasklist_shape_border_color_urgent = red
-- separator
theme.tasklist_sep_fg = default_fg
theme.tasklist_sep_bg = default_bg
theme.tasklist_sep_spacing = 5
-- theme.tasklist_sep = "󱋱"
theme.tasklist_sep = ""
-- max size of an item in tasklist
theme.tasklist_shape_max_size = 200
-- shape of the item
theme.tasklist_shape = function(cr, w, h)
	gears.shape.rounded_rect(cr, w, h, 0)
end

-- theme.wibar_bg = "transparent"
theme.wibar_bg = default_bg
theme.wibar_height = dpi(25)
theme.wibar_border_width = 4
-- theme.wibar_border_color = black

theme.taglist_tags = { ":a", ":b", ":c", ":d", ":e", ":f" }
-- theme.taglist_tags = { " 󰲠 ", " 󰎧 ", " 󱂊 ", " 󰼒 ", " 󱫀 "}
theme.taglist_tag_width = 30
theme.taglist_font = 9
theme.taglist_fg_focus = magenta
theme.taglist_bg_focus = default_bg
theme.taglist_underline_width = dpi(2)
theme.taglist_underline_selected = theme.taglist_fg_focus
theme.taglist_bg_empty = default_bg
theme.taglist_fg_empty = lightgray
theme.taglist_bg_occupied = default_bg
theme.taglist_fg_occupied = default_fg
theme.taglist_bg_urgent = default_bg
theme.taglist_fg_urgent = red
theme.taglist_bg_volatile = default_bg
theme.taglist_fg_volatile = magenta
theme.taglist_spacing = dpi(0)
theme.taglist_shape = function(cr, w, h)
	gears.shape.rounded_rect(cr, w, theme.wibar_height, 0)
end
-- local taglist_square_size = dpi(6)
-- theme.taglist_squares_sel = theme_assets.taglist_squares_sel(taglist_square_size, blue)
-- -- theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(taglist_square_size, shade2)
-- theme.taglist_squares_unsel = theme_assets.taglist_squares_sel(taglist_square_size, blue)

theme.widget_clock = orange
theme.widget_clock_bg = default_bg
theme.widget_clock_border_color = theme.widget_clock
theme.widget_clock_border_width = dpi(2)
theme.widget_clock_fmt = "%d %b %H:%M"
theme.widget_clock_width = 95
theme.widget_cpu = red
theme.widget_cpu_bg = default_bg
theme.widget_cpu_border_color = theme.widget_cpu
theme.widget_cpu_border_width = dpi(2)
theme.widget_cpu_width = 45
theme.widget_mem = cyan
theme.widget_mem_bg = default_bg
theme.widget_mem_border_color = theme.widget_mem
theme.widget_mem_border_width = dpi(2)
theme.widget_mem_width = 60
theme.widget_temp = red
theme.widget_temp_border_color = theme.widget_cpu
theme.widget_temp_border_width = dpi(2)
theme.widget_temp_width = 50
theme.widget_temp_bg = default_bg
theme.widget_updates = green
theme.widget_updates_bg = default_bg
theme.widget_updates_border_color = theme.widget_updates
theme.widget_updates_width = 40
theme.widget_updates_border_width = dpi(2)
theme.widget_vpn = yellow
theme.widget_vpn_bg = default_bg
theme.widget_vpn_border_color = theme.widget_vpn
theme.widget_vpn_border_width = dpi(2)
theme.widget_torrent = yellow
theme.widget_torrent_bg = default_bg
theme.widget_torrent_border_color = theme.widget_torrent
theme.widget_torrent_border_width = dpi(2)

theme.hotkeys_bg = default_bg
theme.hotkeys_fg = default_fg
theme.hotkeys_border_width = dpi(2)
theme.hotkeys_modifiers_fg = cyan
theme.hotkeys_font = font_name .. " 9"
theme.hotkeys_description_font = font_name .. " 9"
theme.hotkeys_group_margin = 20

-- theme.menu_submenu_icon = themes_path .. "default/icons/submenu.png"
theme.menu_height = dpi(25)
theme.menu_width = dpi(100)
theme.menu_font = font_name .. " 9"
-- theme.menu_border_color = "transparent"
theme.menu_border_color = default_bg
theme.menu_separator_color = default_fg
theme.menu_separator_height = dpi(1)

-- Define the image to load
theme.titlebar_close_button_normal = themes_path .. "default/titlebar/close_normal.png"
theme.titlebar_close_button_focus = themes_path .. "default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path .. "default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus = themes_path .. "default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path .. "default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = themes_path .. "default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path .. "default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = themes_path .. "default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path .. "default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive = themes_path .. "default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path .. "default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active = themes_path .. "default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path .. "default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive = themes_path .. "default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path .. "default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = themes_path .. "default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path .. "default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive = themes_path .. "default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path .. "default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active = themes_path .. "default/titlebar/maximized_focus_active.png"

-- using nitrogen instead
-- theme.wallpaper = themes_path .. "default/background.png"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path .. "default/layouts/fairhw.png"
theme.layout_fairv = themes_path .. "default/layouts/fairv_col.png"
theme.layout_floating = themes_path .. "default/layouts/floating_col.png"
theme.layout_magnifier = themes_path .. "default/layouts/magnifierw.png"
theme.layout_max = themes_path .. "default/layouts/max_col.png"
theme.layout_fullscreen = themes_path .. "default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path .. "default/layouts/tilebottomw.png"
theme.layout_tileleft = themes_path .. "default/layouts/tileleft_col.png"
theme.layout_tile = themes_path .. "default/layouts/tile_col.png"
theme.layout_tiletop = themes_path .. "default/layouts/tiletopw.png"
theme.layout_spiral = themes_path .. "default/layouts/spiralw.png"
theme.layout_dwindle = themes_path .. "default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path .. "default/layouts/cornernww.png"
theme.layout_cornerne = themes_path .. "default/layouts/cornernew.png"
theme.layout_cornersw = themes_path .. "default/layouts/cornersww.png"
theme.layout_cornerse = themes_path .. "default/layouts/cornersew.png"

-- Generate Awesome icon:
-- theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, theme.bg_focus, theme.fg_focus)
theme.awesome_icon = themes_path .. "default/icons/menu3.png"

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
-- theme.icon_theme = nil
-- TODO: fix below
-- theme.icon_theme = "Papirus"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80

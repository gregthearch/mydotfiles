local wibox = require("wibox")
local beautiful = require("beautiful")
local awful = require("awful")
local naughty = require("naughty")
local inspect = require("helpers.inspect")

local function wrap_widget(w, forced_width, fg, bg, border_width, border_color)
	fg = fg or beautiful.fg_normal
	bg = bg or beautiful.bg_normal
	local res = {
		{
			{
				w,
				valign = "center",
				halign = "center",
				widget = wibox.container.place,
			},
			fg = fg,
			bg = bg,
			layout = wibox.container.background,
		},
		bottom = border_width,
		color = border_color,
		layout = wibox.container.margin,
	}
	if forced_width then
		res = {
			{
				{
					w,
					valign = "center",
					halign = "center",
					widget = wibox.container.place,
				},
				forced_width = forced_width,
				fg = fg,
				bg = bg,
				layout = wibox.container.background,
			},
			bottom = border_width,
			color = border_color,
			layout = wibox.container.margin,
		}
	end
	return res
end

local function task_menu_items(c)
	local header_entry = {
		c.pid .. ":" .. c.name,
		function()
			local notif_obj = {
				name = c.name,
				pid = c.pid,
				class = c.class,
				instance = c.instance,
				icon_name = c.icon_name,
				window = c.window,
				type = c.type,
				role = c.role,
				motif_wm_hints = c.motif_wm_hints,
			}
			naughty.notify({ position = "top_right", timeout = 30, text = inspect(notif_obj, { depth = 3 }) })
		end,
	}
	local reset_entry = {
		"󰦛  reset",
		function()
			if c.maximized then
				c.maximized = false
			elseif c.minimized then
				c.minimized = false
			elseif c.floating then
				c.floating = false
			end
		end,
	}
	local float_entry = {
		"󰉨  float toggle",
		function()
			c.floating = not c.floating
		end,
	}
	local minimize_entry = {
		"󰁄  minimize toggle",
		function()
			c.minimized = not c.minimized
		end,
	}
	local maximize_entry = {
		"󰁌  maximize toggle",
		function()
			c.maximized = not c.maximized
		end,
	}
	local kill_entry = {
		"  kill",
		function()
			c:kill()
		end,
	}
	local master_entry = {
		"󱥩  set master",
		function()
			awful.client.setmaster(c)
		end,
	}
	local sticky_entry = {
		"  sticky toggle",
		function()
			if c.sticky then
				c.sticky = false
			else
				c.sticky = true
			end
		end,
	}
	local menu_items = {
		header_entry,
		kill_entry,
		-- reset_entry,
		master_entry,
		maximize_entry,
		minimize_entry,
		float_entry,
		sticky_entry,
	}
	for i = 1, #beautiful.taglist_tags do
		local tag_index = c.first_tag.index
		if tag_index ~= i then
			local move_item = {
				"󰧆  send to tag [" .. c.screen.tags[i].name .. "]",
				function()
					local t = c.screen.tags[i]
					c:move_to_tag(t)
					awful.tag.viewonly(t)
				end,
			}
			table.insert(menu_items, move_item)
		end
	end
	return menu_items
end

return {
	wrap_widget = wrap_widget,
	task_menu_items = task_menu_items,
}

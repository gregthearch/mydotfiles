-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- -- Widget and layout library
local wibox = require("wibox")
-- -- Theme handling library
local beautiful = require("beautiful")
-- -- Notification library
local naughty = require("naughty")
-- local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local lain = require("lain")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Helper functions
local inspect = require("helpers.inspect")
local click_to_hide = require("helpers.click_to_hide")
local my_func = require("helpers.my_functions")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({
		preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors,
	})
end

-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function(err)
		-- Make sure we don't go into an endless error loop
		if in_error then
			return
		end
		in_error = true

		naughty.notify({
			preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err),
		})
		in_error = false
	end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/default/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "st"
browser = "brave"
files = "thunar"
app_menu = "rofi -show drun -show-icons"
run_menu = "rofi -show run -show-icons"
window_select = "rofi -show window -show-icons"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
	awful.layout.suit.tile,
	-- awful.layout.suit.tile.left,
	awful.layout.suit.fair,
	awful.layout.suit.floating,
	-- awful.layout.suit.tile.bottom,
	-- awful.layout.suit.tile.top,
	-- awful.layout.suit.fair.horizontal,
	-- awful.layout.suit.spiral,
	-- awful.layout.suit.spiral.dwindle,
	awful.layout.suit.max,
	-- awful.layout.suit.max.fullscreen,
	-- awful.layout.suit.magnifier,
	-- awful.layout.suit.corner.nw,
	-- awful.layout.suit.corner.ne,
	-- awful.layout.suit.corner.sw,
	-- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
-- TODO: lf not launched in zsh
mymainmenu = awful.menu({
	theme = { width = 160, font = beautiful.font },
	items = {
		{ "󱓞  apps", app_menu },
		{ "󰞷  terminal", terminal },
		{ "󰙅  files", files },
		{ "󰖟  browser", browser },
		{ "󰽘  windows", window_select },
		{ "  reset torrent", terminal .. " -e sudo systemctl restart qbittorrent-nox@do.service" },
		-- { "󰲛  vpn down", terminal .. " -e sudo nmcli connection down str-syd102_a363893" },
		-- { "󰛴  vpn up", terminal .. " -e sudo nmcli connection up str-syd102_a363893" },
		{ "  run", run_menu },
		{
			"󰘳  hotkeys",
			function()
				hotkeys_popup.show_help(nil, awful.screen.focused())
			end,
		},
		{ "  restart", awesome.restart },
		{
			"󰿅  quit",
			function()
				awesome.quit()
			end,
		},
	},
})

-- mymainmenu:get_root().wibox:connect_signal("mouse::leave", function(c)
-- 	mymainmenu:hide()
-- end)
click_to_hide.menu(mymainmenu, nil, true)

mylauncher = awful.widget.launcher({
	image = beautiful.awesome_icon,
	menu = mymainmenu,
})

-- Menubar configuration
-- menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
-- mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget({
	format = beautiful.widget_clock_fmt,
	widget = wibox.widget.textclock,
})

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({}, 1, function(t)
		t:view_only()
	end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
			awful.tag.viewonly(t)
		end
	end),
	awful.button({}, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({}, 4, function(t)
		awful.tag.viewnext(t.screen)
	end),
	awful.button({}, 5, function(t)
		awful.tag.viewprev(t.screen)
	end)
)

-- custom code for task menu

local mytaskmenu = awful.menu({ items = {}, theme = { width = 170 } })

-- only_outside = true does not work with this menu
click_to_hide.menu(mytaskmenu, nil, false)

local tasklist_buttons = gears.table.join(
	awful.button({}, 1, function(c)
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal("request::activate", "tasklist", { raise = true })
		end
	end),
	awful.button({}, 3, function(c)
		-- naughty.notify({ position = "top_right", timeout = 30, text = "debug: mytaskmenu show triggered" })
		-- refresh all sub menu entries
		mytaskmenu:hide()
		local menu_items = my_func.task_menu_items(c)
		-- this loop purge all entries
		for i = 1, #menu_items do
			mytaskmenu:delete(1)
		end
		-- this loop re-populate all entries
		for i, v in ipairs(menu_items) do
			mytaskmenu:add(v)
		end
		mytaskmenu:show()
	end),
	awful.button({}, 4, function()
		awful.client.focus.byidx(1)
	end),
	awful.button({}, 5, function()
		awful.client.focus.byidx(-1)
	end)
)

-- taken care of by nitrogen
-- local function set_wallpaper(s)
-- 	-- Wallpaper
-- 	if beautiful.wallpaper then
-- 		local wallpaper = beautiful.wallpaper
-- 		-- If wallpaper is a function, call it with the screen
-- 		if type(wallpaper) == "function" then
-- 			wallpaper = wallpaper(s)
-- 		end
-- 		gears.wallpaper.maximized(wallpaper, s, true)
-- 	end
-- end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
-- screen.connect_signal("property::geometry", set_wallpaper)

local markup = lain.util.markup
local cpu = lain.widget.cpu({
	settings = function()
		widget:set_markup(markup.fontfg(beautiful.font, beautiful.widget_cpu, "󰍛 " .. cpu_now.usage .. "%"))
	end,
})
local memory = lain.widget.mem({
	settings = function()
		widget:set_markup(
			markup.fontfg(
				beautiful.font,
				beautiful.widget_mem,
				"  " .. tonumber(string.format("%.1f", (mem_now.used / 1024))) .. "G"
			)
		)
	end,
})
-- local netdowninfo = wibox.widget.textbox()
-- local netupinfo = lain.widget.net({
-- 	settings = function()
-- 		widget:set_markup(markup.fontfg(beautiful.font, beautiful.widget_net, "󰕒 " .. net_now.sent .. " "))
-- 		netdowninfo:set_markup(markup.fontfg(beautiful.font, beautiful.widget_net, "󰇚 " .. net_now.received .. " "))
-- 	end,
-- })
local temp = lain.widget.temp({
	settings = function()
		widget:set_markup(
			markup.fontfg(beautiful.font, beautiful.widget_temp, " " .. math.floor(coretemp_now) .. "°C")
		)
	end,
})
local torrent = wibox.widget({
	awful.widget.watch('bash -c "~/.config/awesome/scripts/qbittorrent/qbittorrent"', 10),
	fg = beautiful.widget_torrent,
	widget = wibox.container.background,
})

-- custom function created to determine the underline color in tasklist
-- TODO: fix error on line 276
local function get_underline_color(c) --luacheck: no unused args
	if c.urgent then
		return beautiful.tasklist_shape_border_color_urgent
	elseif c.minimized then
		return beautiful.tasklist_shape_border_color_minimized
	elseif c.window == client.focus.window then
		return beautiful.tasklist_shape_border_color_focus
	else
		return beautiful.tasklist_shape_border_color
	end
end

awful.screen.connect_for_each_screen(function(s)
	-- Wallpaper already set by nitrogen
	-- set_wallpaper(s)

	-- Each screen has its own tag table.
	awful.tag(beautiful.taglist_tags, s, awful.layout.layouts[1])

	-- Create a promptbox for each screen
	-- s.mypromptbox = awful.widget.prompt()

	-- Create an imagebox widget which will contain an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
		awful.button({}, 1, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 3, function()
			awful.layout.inc(-1)
		end),
		awful.button({}, 4, function()
			awful.layout.inc(1)
		end),
		awful.button({}, 5, function()
			awful.layout.inc(-1)
		end)
	))

	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist({
		screen = s,
		filter = awful.widget.taglist.filter.all,
		widget_template = {
			{
				{
					{
						id = "text_role",
						widget = wibox.widget.textbox,
					},
					halign = "center",
					valign = "center",
					widget = wibox.container.place,
					-- layout = wibox.layout.fixed.horizontal,
				},
				id = "custom_role",
				forced_width = beautiful.taglist_tag_width,
				bottom = beautiful.taglist_underline_width,
				widget = wibox.container.margin,
			},
			id = "background_role",
			widget = wibox.container.background,
			create_callback = function(self, c3, index, objects) --luacheck: no unused args
				if c3.selected then
					self:get_children_by_id("custom_role")[1].color = beautiful.taglist_underline_selected
				else
					self:get_children_by_id("custom_role")[1].color = "transparent"
				end
			end,
			update_callback = function(self, c3, index, objects) --luacheck: no unused args
				if c3.selected then
					self:get_children_by_id("custom_role")[1].color = beautiful.taglist_underline_selected
				else
					self:get_children_by_id("custom_role")[1].color = "transparent"
				end
			end,
		},
		buttons = taglist_buttons,
	})
	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist({
		screen = s,
		filter = awful.widget.tasklist.filter.currenttags,
		buttons = tasklist_buttons,
		layout = {
			spacing = beautiful.tasklist_sep_spacing,
			max_widget_size = beautiful.tasklist_shape_max_size,
			spacing_widget = {
				{
					{
						text = beautiful.tasklist_sep,
						widget = wibox.widget.textbox,
					},
					halign = "center",
					widget = wibox.container.place,
				},
				fg = beautiful.tasklist_sep_fg,
				bg = beautiful.tasklist_sep_bg,
				widget = wibox.container.background,
			},
			layout = wibox.layout.flex.horizontal,
		},
		widget_template = {
			{
				{
					{
						{
							id = "icon_role",
							widget = wibox.widget.imagebox,
						},
						margins = 4,
						widget = wibox.container.margin,
					},
					{
						id = "text_role",
						widget = wibox.widget.textbox,
					},
					layout = wibox.layout.fixed.horizontal,
				},
				id = "custom_underline",
				bottom = beautiful.tasklist_underline_width,
				widget = wibox.container.margin,
			},
			id = "background_role",
			widget = wibox.container.background,
			create_callback = function(self, c, index, objects) --luacheck: no unused args
				self:get_children_by_id("icon_role")[1].client = c

				local task = self:get_children_by_id("custom_underline")[1]
				task.color = get_underline_color(c)
			end,
			update_callback = function(self, c, index, objects) --luacheck: no unused args
				self:get_children_by_id("icon_role")[1].client = c

				local task = self:get_children_by_id("custom_underline")[1]
				task.color = get_underline_color(c)
			end,
		},
	})

	-- Create a seperator
	s.mysep = wibox.widget.textbox(" ")
	-- s.mysep = wibox.widget.textbox("  ")

	-- Create the wibox
	s.mywibox = awful.wibar({ position = "top", screen = s })

	-- Add buttons to the wibox
	-- Below help with click_to_hide but breaks the systray widget
	-- s.mywibox:buttons(awful.util.table.join(awful.button({}, 1, function() end)))

	-- Add widgets to the wibox
	s.mywibox:setup({
		layout = wibox.layout.align.horizontal,
		{ -- Left widgets
			layout = wibox.layout.fixed.horizontal,
			mylauncher,
			s.mysep,
			s.mytaglist,
			s.mysep,
		},
		{
			s.mytasklist, -- Middle widget
			-- bg = beautiful.tasklist_bg_normal,
			layout = wibox.container.background,
		},
		{ -- Right widgets
			layout = wibox.layout.fixed.horizontal,
			s.mysep,
			-- Below commented out as functionality redundant with systray
			-- my_func.wrap_widget(
			-- 	awful.widget.watch('bash -c "~/.config/awesome/scripts/openvpn-status"', 10),
			-- 	nil,
			-- 	beautiful.widget_vpn,
			-- 	beautiful.widget_vpn_bg,
			-- 	beautiful.widget_vpn_border_width,
			-- 	beautiful.widget_vpn_border_color
			-- ),
			my_func.wrap_widget(
				torrent,
				nil,
				beautiful.widget_torrent,
				beautiful.widget_torrent_bg,
				beautiful.widget_torrent_border_width,
				beautiful.widget_torrent_border_color
			),
			s.mysep,
			my_func.wrap_widget(
				cpu.widget,
				beautiful.widget_cpu_width,
				beautiful.widget_cpu,
				beautiful.widget_cpu_bg,
				beautiful.widget_cpu_border_width,
				beautiful.widget_cpu_border_color
			),
			my_func.wrap_widget(
				temp.widget,
				beautiful.widget_temp_width,
				beautiful.widget_temp,
				beautiful.widget_temp_bg,
				beautiful.widget_temp_border_width,
				beautiful.widget_temp_border_color
			),
			s.mysep,
			my_func.wrap_widget(
				memory.widget,
				beautiful.widget_mem_width,
				beautiful.widget_mem,
				beautiful.widget_mem_bg,
				beautiful.widget_mem_border_width,
				beautiful.widget_mem_border_color
			),
			s.mysep,
			my_func.wrap_widget(
				awful.widget.watch('bash -c "~/.config/awesome/scripts/checkupdates"', 300),
				beautiful.widget_updates_width,
				beautiful.widget_updates,
				beautiful.widget_updates_bg,
				beautiful.widget_updates_border_width,
				beautiful.widget_updates_border_color
			),
			s.mysep,
			my_func.wrap_widget(
				mytextclock,
				beautiful.widget_clock_width,
				beautiful.widget_clock,
				beautiful.widget_clock_bg,
				beautiful.widget_clock_border_width,
				beautiful.widget_clock_border_color
			),
			s.mysep,
			wibox.widget.systray(),
			-- my_func.wrap_widget(wibox.widget.systray(), nil, nil, nil, 0, "transparent"),
			s.mysep,
			s.mylayoutbox,
		},
	})
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
	awful.button({}, 1, function() end), -- required for click_to_hide to work
	awful.button({}, 3, function()
		mymainmenu:show()
	end),
	awful.button({}, 4, awful.tag.viewnext),
	awful.button({}, 5, awful.tag.viewprev)
))

-- }}}

-- {{{ Key bindings
-- TODO: review bindings
globalkeys = gears.table.join(

	awful.key({ modkey }, "h", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),
	awful.key({ modkey }, "[", awful.tag.viewprev, { description = "view previous", group = "tag" }),
	awful.key({ modkey }, "]", awful.tag.viewnext, { description = "view next", group = "tag" }),
	awful.key({ modkey, "Shift" }, "Tab", awful.tag.history.restore, { description = "go back", group = "tag" }),

	awful.key({ modkey }, ".", function()
		awful.client.focus.byidx(1)
	end, { description = "focus next", group = "client" }),
	awful.key({ modkey }, ",", function()
		awful.client.focus.byidx(-1)
	end, { description = "focus previous", group = "client" }),
	awful.key({ modkey }, "Right", function()
		awful.client.focus.bydirection("right")
	end, { description = "focus right", group = "client" }),
	awful.key({ modkey }, "Left", function()
		awful.client.focus.bydirection("left")
	end, { description = "focus left", group = "client" }),
	awful.key({ modkey }, "Up", function()
		awful.client.focus.bydirection("up")
	end, { description = "focus up", group = "client" }),
	awful.key({ modkey }, "Down", function()
		awful.client.focus.bydirection("down")
	end, { description = "focus down", group = "client" }),
	-- awful.key({ modkey }, "w", function()
	-- 	mymainmenu:show()
	-- end, { description = "show main menu", group = "awesome" }),

	-- Layout manipulation
	awful.key({ modkey, "Control" }, "Right", function()
		awful.client.swap.bydirection("right")
	end, { description = "swap with client to the right", group = "client" }),
	awful.key({ modkey, "Control" }, "Left", function()
		awful.client.swap.bydirection("left")
	end, { description = "swap with client to the left", group = "client" }),
	awful.key({ modkey, "Control" }, "Up", function()
		awful.client.swap.bydirection("up")
	end, { description = "swap with upper client", group = "client" }),
	awful.key({ modkey, "Control" }, "Down", function()
		awful.client.swap.bydirection("down")
	end, { description = "swap with lower client", group = "client" }),
	awful.key({ modkey }, "u", awful.client.urgent.jumpto, { description = "jump to urgent client", group = "client" }),
	awful.key({ modkey }, "Tab", function()
		awful.client.focus.history.previous()
		if client.focus then
			client.focus:raise()
		end
	end, { description = "go back", group = "client" }),

	-- Standard program
	awful.key({ modkey }, "Return", function()
		awful.spawn(terminal)
	end, { description = "open terminal", group = "launcher" }),
	awful.key({ modkey }, "r", function()
		awful.spawn(terminal .. " -e lf")
	end, { description = "open file manager (terminal)", group = "launcher" }),
	awful.key({ modkey }, "b", function()
		awful.spawn(browser)
	end, { description = "open web browser", group = "launcher" }),
	awful.key({ modkey }, "space", function()
		awful.spawn(app_menu)
	end, { description = "app launcher (rofi)", group = "launcher" }),
	awful.key({ modkey }, "w", function()
		awful.spawn(window_select)
	end, { description = "window picker (rofi)", group = "client" }),
	awful.key({ modkey, "Control" }, "r", awesome.restart, { description = "reload awesome", group = "awesome" }),
	awful.key({ modkey, "Control" }, "q", awesome.quit, { description = "quit awesome", group = "awesome" }),

	awful.key({ modkey, "Shift" }, "Right", function()
		awful.tag.incmwfact(0.05)
		-- awful.client.incwfact(0.05)
	end, { description = "increase master width factor", group = "client" }),
	awful.key({ modkey, "Shift" }, "Left", function()
		awful.tag.incmwfact(-0.05)
		-- awful.client.incwfact(-0.05)
	end, { description = "decrease master width factor", group = "client" }),
	awful.key({ modkey, "Shift" }, "Up", function()
		-- awful.tag.incmwfact(0.05)
		awful.client.incwfact(0.05)
	end, { description = "increase client height factor", group = "client" }),
	awful.key({ modkey, "Shift" }, "Down", function()
		-- awful.tag.incmwfact(-0.05)
		awful.client.incwfact(-0.05)
	end, { description = "decrease client height factor", group = "client" }),
	awful.key({ modkey, "Shift" }, "h", function()
		awful.tag.incnmaster(1, nil, true)
	end, { description = "increase the number of master clients", group = "layout" }),
	awful.key({ modkey, "Shift" }, "l", function()
		awful.tag.incnmaster(-1, nil, true)
	end, { description = "decrease the number of master clients", group = "layout" }),
	awful.key({ modkey, "Control" }, "h", function()
		awful.tag.incncol(1, nil, true)
	end, { description = "increase the number of columns", group = "layout" }),
	awful.key({ modkey, "Control" }, "l", function()
		awful.tag.incncol(-1, nil, true)
	end, { description = "decrease the number of columns", group = "layout" }),
	-- awful.key({ modkey }, "space", function()
	-- 	awful.layout.inc(1)
	-- end, { description = "select next", group = "layout" }),
	awful.key({ modkey }, "l", function()
		awful.layout.inc(1)
	end, { description = "select next layout", group = "layout" }),

	awful.key({ modkey, "Control" }, "n", function()
		local c = awful.client.restore()
		-- Focus restored client
		if c then
			c:emit_signal("request::activate", "key.unminimize", { raise = true })
		end
	end, { description = "restore minimized", group = "client" })

	-- Prompt
	-- awful.key({ modkey }, "r", function()
	-- 	awful.screen.focused().mypromptbox:run()
	-- end, { description = "run prompt", group = "launcher" }),

	-- awful.key({ modkey }, "x", function()
	-- 	awful.prompt.run({
	-- 		prompt = "Run Lua code: ",
	-- 		textbox = awful.screen.focused().mypromptbox.widget,
	-- 		exe_callback = awful.util.eval,
	-- 		history_path = awful.util.get_cache_dir() .. "/history_eval",
	-- 	})
	-- end, { description = "lua execute prompt", group = "awesome" }),
	-- Menubar
	-- awful.key({ modkey }, "p", function()
	-- 	menubar.show()
	-- end, { description = "show the menubar", group = "launcher" })
)

clientkeys = gears.table.join(
	awful.key({ modkey }, "f", function(c)
		c.fullscreen = not c.fullscreen
		c:raise()
	end, { description = "toggle fullscreen", group = "client" }),
	awful.key({ modkey }, "q", function(c)
		c:kill()
	end, { description = "quit", group = "client" }),
	awful.key(
		{ modkey, "Control" },
		"space",
		awful.client.floating.toggle,
		{ description = "toggle floating", group = "client" }
	),
	awful.key({ modkey, "Control" }, "Return", function(c)
		c:swap(awful.client.getmaster())
	end, { description = "move to master", group = "client" }),
	-- awful.key({ modkey }, "o", function(c)
	-- 	c:move_to_screen()
	-- end, { description = "move to screen", group = "client" }),
	awful.key({ modkey }, "t", function(c)
		c.ontop = not c.ontop
	end, { description = "toggle keep on top", group = "client" }),
	awful.key({ modkey }, "n", function(c)
		-- The client currently has the input focus, so it cannot be
		-- minimized, since minimized clients can't have the focus.
		c.minimized = true
	end, { description = "minimize", group = "client" }),
	awful.key({ modkey }, "m", function(c)
		c.maximized = not c.maximized
		c:raise()
	end, { description = "(un)maximize", group = "client" }),
	awful.key({ modkey, "Control" }, "m", function(c)
		c.maximized_vertical = not c.maximized_vertical
		c:raise()
	end, { description = "(un)maximize vertically", group = "client" }),
	awful.key({ modkey, "Shift" }, "m", function(c)
		c.maximized_horizontal = not c.maximized_horizontal
		c:raise()
	end, { description = "(un)maximize horizontally", group = "client" })
)
-- awful.key({ modkey }, "left", function()
-- 	awful.client.focus.bydirection("left")
-- 	if client.focus then
-- 		client.focus:raise()
-- 	end
-- end)
-- awful.key({ modkey }, "down", function()
-- 	awful.client.focus.bydirection("down")
-- 	if client.focus then
-- 		client.focus:raise()
-- 	end
-- end)
-- awful.key({ modkey }, "up", function()
-- 	awful.client.focus.bydirection("up")
-- 	if client.focus then
-- 		client.focus:raise()
-- 	end
-- end)
-- awful.key({ modkey }, "right", function()
-- 	awful.client.focus.bydirection("right")
-- 	if client.focus then
-- 		client.focus:raise()
-- 	end
-- end)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, #beautiful.taglist_tags do
	globalkeys = gears.table.join(
		globalkeys,
		-- View tag only.
		awful.key({ modkey }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
		end, { description = "view tag #" .. i, group = "tag" }),
		-- Toggle tag display.
		-- awful.key({ modkey, "Control" }, "#" .. i + 9, function()
		-- 	local screen = awful.screen.focused()
		-- 	local tag = screen.tags[i]
		-- 	if tag then
		-- 		awful.tag.viewtoggle(tag)
		-- 	end
		-- end, { description = "toggle tag #" .. i, group = "tag" }),
		-- Move client to tag.
		awful.key({ modkey, "Control" }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
					awful.tag.viewonly(tag)
				end
			end
		end, { description = "move focused client to tag #" .. i, group = "tag" })
		-- Toggle tag on focused client.
		-- awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9, function()
		-- 	if client.focus then
		-- 		local tag = client.focus.screen.tags[i]
		-- 		if tag then
		-- 			client.focus:toggle_tag(tag)
		-- 		end
		-- 	end
		-- end, { description = "toggle focused client on tag #" .. i, group = "tag" })
	)
end

clientbuttons = gears.table.join(
	awful.button({}, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
	end),
	-- swapped 1 and 3
	awful.button({ modkey }, 3, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.move(c)
	end),
	awful.button({ modkey }, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.resize(c)
	end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
		},
	},

	-- Floating clients.
	{
		rule_any = {
			instance = {
				"DTA", -- Firefox addon DownThemAll.
				"copyq", -- Includes session name in class.
				"pinentry",
				"nm-connection-editor",
			},
			class = {
				"Arandr",
				"Blueman-manager",
				"Gpick",
				"Kruler",
				"MessageWin", -- kalarm.
				"Sxiv",
				"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
				"Wpa_gui",
				"veromix",
				"xtightvncviewer",
			},

			-- Note that the name property shown in xprop might be set slightly after creation of the client
			-- and the name shown there might not match defined rules here.
			name = {
				"Event Tester", -- xev.
			},
			role = {
				"AlarmWindow", -- Thunderbird's calendar.
				"ConfigManager", -- Thunderbird's about:config.
				"pop-up", -- e.g. Google Chrome's (detached) Developer Tools.
			},
		},
		properties = { floating = true },
	},

	-- Add titlebars to normal clients and dialogs
	--  DO: set titlebars_enabled = false
	{ rule_any = { type = { "normal", "dialog" } }, properties = { titlebars_enabled = false } },

	-- Set Firefox to always map on the tag named "2" on screen 1.
	-- { rule = { class = "Firefox" },
	--   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	if not awesome.startup then
		awful.client.setslave(c)
	end

	if awesome.startup and not c.size_hints.user_position and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
	-- buttons for the titlebar
	local buttons = gears.table.join(
		awful.button({}, 1, function()
			c:emit_signal("request::activate", "titlebar", { raise = true })
			awful.mouse.client.move(c)
		end),
		awful.button({}, 3, function()
			c:emit_signal("request::activate", "titlebar", { raise = true })
			awful.mouse.client.resize(c)
		end)
	)

	awful.titlebar(c):setup({
		{ -- Left
			awful.titlebar.widget.iconwidget(c),
			buttons = buttons,
			layout = wibox.layout.fixed.horizontal,
		},
		{ -- Middle
			{ -- Title
				align = "center",
				widget = awful.titlebar.widget.titlewidget(c),
			},
			buttons = buttons,
			layout = wibox.layout.flex.horizontal,
		},
		{ -- Right
			awful.titlebar.widget.floatingbutton(c),
			awful.titlebar.widget.maximizedbutton(c),
			awful.titlebar.widget.stickybutton(c),
			awful.titlebar.widget.ontopbutton(c),
			awful.titlebar.widget.closebutton(c),
			layout = wibox.layout.fixed.horizontal(),
		},
		layout = wibox.layout.align.horizontal,
	})
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", { raise = false })
end)

client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
end)
-- }}}

-- Rounded corners
-- client.connect_signal("manage", function(c)
-- 	c.shape = function(cr, w, h)
-- 		gears.shape.rounded_rect(cr, w, h, 10)
-- 	end
-- end)
--
-- beautiful.notification_shape = function(cr, w, h)
-- 	gears.shape.rounded_rect(cr, w, h, 10)
-- end

-- -- Autorun programs
awful.spawn("picom", false)
awful.spawn("nm-applet --indicator", false)
awful.spawn("nitrogen --restore", false)

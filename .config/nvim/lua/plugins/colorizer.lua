return {
  {
    "NvChad/nvim-colorizer.lua",
    lazy = true,
    event = { "BufReadPre", "BufNewFile" },
    opts = {
      user_default_options = {
        tailwind = true,
        mode = "background", -- other options: "background", "foreground", "virtualtext"
        virtualtext = "",
      },
    },
  },
}

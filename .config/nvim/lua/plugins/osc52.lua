return {
  {
    "ojroques/nvim-osc52",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
      vim.keymap.set(
        "n",
        "<leader>yc",
        require("osc52").copy_operator,
        { expr = true, desc = "OSC52 copy text to clipboard" }
      )
      vim.keymap.set("n", "<leader>yy", "<leader>yc_", { remap = true, desc = "OSC52 copy line" })
      vim.keymap.set("v", "<leader>y", require("osc52").copy_visual, { desc = "OSC52 copy selection" })
    end,
  },
}

#!/bin/bash

killall -q picom
killall -q nm-applet
killall -q dwmblocks
killall -q dunst

picom &
nitrogen --restore &
nm-applet --indicator &
dunst &
dwmblocks &
